-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
--require "mobdebug".start()

-- Namespaces
Classes = {}
Config = {}
Enums = {}

-- Enums
NodeEnum = {
  AVAIBLE_TO_GO = 0,
  UNAVAIBLE_TO_GO = 1,
  PLAYER = 2,
  GOAL = 3
}

-- Requires
require "src.Classes.ArrayClass"
require "src.Classes.NodeClass"
require "src.Classes.FieldClass"
require "src.config"

-- Your code here
local NodeClass = Classes.Node
local FieldClass = Classes.Field
local ArrayClass = Classes.Array

local field = FieldClass:new(Config.GameConfig.field)
field:translate(20, 100)

local player = NodeClass:new(NodeEnum.PLAYER, 3, 1, field.group)
local goal = NodeClass:new(NodeEnum.GOAL, 1, 4, field.group)
player.view:scale(0.85, 0.85)
goal.view:scale(0.85, 0.85)

field:init(player, goal)

local function Go( event )
  local time = system.getTimer()
  field:go()

  print("Speed of algorithm is: "..system.getTimer() - time.." ms")

  return true
end

Runtime:addEventListener("tap", Go)