Classes.Node = {}
local Node = Classes.Node

-- Optimal node formula is F = G + H
-- G is path from start point
-- H is path to goal point

local RECT_SIDE = 30
local STROKE = 5
local HORIZONTAL_COST = 10
local VERTICAL_COST = 14

function Node:new(value, row, column, group)
  local nodeObj = {}

  self.__index = self
  setmetatable(nodeObj, self)

  self.__tostring = function() return "[ row: "..self.row..", column :"..self.column.." ]" end

  nodeObj.row = row
  nodeObj.column = column
  nodeObj.value = value
  nodeObj.view = display.newRect( (column - 1) * RECT_SIDE, (row - 1) * RECT_SIDE,
                                  RECT_SIDE - STROKE, RECT_SIDE - STROKE )

  if (value == NodeEnum.AVAIBLE_TO_GO) then
    nodeObj.view:setFillColor(1, 1, 1)
  elseif (value == NodeEnum.UNAVAIBLE_TO_GO) then
    nodeObj.view:setFillColor(0, 0, 1)
  elseif (value == NodeEnum.PLAYER) then
    nodeObj.view:setFillColor(1, 0, 0)
  elseif (value == NodeEnum.GOAL) then
    nodeObj.view:setFillColor(1, 1, 0)
  else
    error("Put correct NodeEnum value to the constructor")
  end

  if group then
    group:insert(nodeObj.view)
  end

  return nodeObj
end

function Node:setRow(row)
  local this = self

  this.row = row
  this.view.x = (row - 1) * RECT_SIDE
end

function Node:setColumn(column)
  local this = self

  this.column = column
  this.view.y = (column - 1) * RECT_SIDE
end

function Node:getAvaibility()
  local this = self

  if this.value == NodeEnum.AVAIBLE_TO_GO then
    return true
  else
    return false
  end
end

function Node:disable()
  local this = self

  this.value = NodeEnum.UNAVAIBLE_TO_GO
end

function Node:getCostFromStartPoint(startRow, startColumn)
  local this = self

  local horizontalCost = math.abs(this.row - startRow) * HORIZONTAL_COST
  local verticalCost = math.abs(this.column - startColumn) * VERTICAL_COST
  local cost = horizontalCost + verticalCost

  print("Cost From Start point: ", cost)
  return cost
end

function Node:getCostToGoalPoint(goalRow, goalColumn)
  local this = self

  local horizontalCost = math.abs(goalRow - this.row) * HORIZONTAL_COST
  local verticalCost = math.abs(goalColumn - this.column) * VERTICAL_COST
  local cost = horizontalCost + verticalCost

  print("Cost To Goal Point: ", cost)
  return cost
end

function Node:getCost(start, goal)
  local this = self
  local startRow = start.row
  local startColumn = start.column
  local goalRow = goal.row
  local goalColumn = goal.column

  local cost = this:getCostFromStartPoint(startRow, startColumn) + this:getCostToGoalPoint(goalRow, goalColumn)
  print("Cost is "..cost)

  return cost
end

return Node