Classes.Field = {}
local Field = Classes.Field
local Node = Classes.Node
local Array = Classes.Array

function Field:new(fieldMatrix, x, y)
  local fieldObj = {}

  fieldObj.rows = #fieldMatrix

  for k = 2, #fieldMatrix do
    if #fieldMatrix[k] > #fieldMatrix[k - 1] then
      fieldObj.columns = #fieldMatrix[k]
    end
  end

  self.__index = self
  setmetatable(fieldObj, self)

  fieldObj.openList = Array:new()
  fieldObj.closeList = Array:new()

  fieldObj.group = display.newGroup()

  for i = 1, #fieldMatrix do
    fieldObj[i] = {}
    for j = 1, #fieldMatrix[i] do
      if (fieldMatrix[i][j] == 0) then
        fieldObj[i][j] = Node:new(NodeEnum.UNAVAIBLE_TO_GO, i, j, fieldObj.group)
      elseif (fieldMatrix[i][j] == 1) then
        fieldObj[i][j] = Node:new(NodeEnum.AVAIBLE_TO_GO, i, j, fieldObj.group)
      else
        fieldMatrix[i][j] = nil
        --error("Put correct bit matrix to the constructor")
      end
    end
  end

  return fieldObj
end

function Field:translate(x, y)
  local this = self

  this.group:translate(x, y)
end

function Field:addToOpenList(row, column)
  local this = self
  local added = this[row][column]

  if this.openList:find(this[row][column]) > 0 then
    return
  end

  added.view:setFillColor(0.6, 1, 0.6)
  this.openList:add( this[row][column] )
end

function Field:addToCloseList(row, column)
  local this = self
  local added = this[row][column]

  if this.closeList:find(this[row][column]) > 0 then
    return
  end

  added.view:setFillColor(0.7, 0.7, 1)
  this.openList:remove( this[row][column] )
  this.closeList:add( this[row][column] )
  this[row][column]:disable()
end

function Field:init(player, goal)
  local this = self

  this.start = this[player.row][player.column]
  this.lastCheck = this.start
  this.goal = this[goal.row][goal.column]

  this:addToOpenList( this.start.row, this.start.column)
  this:step()
end

function Field:displayParents(node)
  while node.parent ~= nil do
    node.view:setFillColor(1, 0.5, 0.1)
    node = node.parent
  end
end

function Field:go()
  local this = self
  local selectedNode = this.openList[1]

  if selectedNode == nil then
    local text = display.newText(
      {
        x = display.contentCenterX,
        y = display.contentCenterY,
        text = "NO WAY!",
        fontSize = 80
      }
    )
    text:setFillColor(1, 0, 0)
    return
  end

  local selectedCost = selectedNode:getCost(this.start, this.goal)

  for i = 2, this.openList:getLength() do
      if (this.openList[i]:getCost(this.start, this.goal) < selectedCost) then
        selectedNode = this.openList[i]
        selectedCost = selectedNode:getCost(this.start, this.goal)
      end
  end
  this:addToCloseList(this.lastCheck.row, this.lastCheck.column)
  print("WE Select "..selectedCost)

  if selectedNode.row == this.goal.row and selectedNode.column == this.goal.column then
    this:displayParents(selectedNode)
    print("WE FIND THE WAY!!!")
    return
  end

  this.lastCheck = selectedNode
  --this:addToOpenList(selectedNode.row, selectedNode.column)
  this:step()

  --this:go()
  timer.performWithDelay(5, function() this:go() end)
end

function Field:step()
  local this = self

  local currentRow = this.lastCheck.row
  local currentColumn = this.lastCheck.column

  local sidesRow = {1, -1, 0, 0}
  local sidesColumn = {0, 0, 1,-1}

  for i = 1, #sidesRow do
      local checkingRow = currentRow + sidesRow[i]
      local checkingColumn = currentColumn + sidesColumn[i]

      if (checkingRow >= 1 and checkingRow <= this.rows) and
         (checkingColumn >= 1 and checkingColumn <= this.columns) and
         this[checkingRow][checkingColumn] and
         this[checkingRow][checkingColumn]:getAvaibility()
      then
          this:addToOpenList(checkingRow, checkingColumn)
          this[checkingRow][checkingColumn].parent = this.lastCheck
      end
  end

  this:addToCloseList(this.lastCheck.row, this.lastCheck.column)
end

return Field