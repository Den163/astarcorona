Classes.Array = {}
local Array = Classes.Array

function Array:new()
  local arrayObj = {}

  self.__index = self
  setmetatable(arrayObj, self)

  arrayObj.elements = 0

  return arrayObj
end

function Array:add(element)
  local this = self

  this.elements = this.elements + 1

  this[this.elements] = element
end

function Array:remove(element)
  local this = self

  for i = this.elements, 1, -1 do
    if (this[i] == element) then
      for k = i, this.elements - 1 do
        this[k] = this[k + 1]
      end

      this[this.elements] = nil
      this.elements = this.elements - 1
      return true
    end
  end

  return false
end

function Array:find(element)
  local this = self

  for i = 1, this.elements do
    if this[i] == element then
      return i
    end
  end

  return -1
end

function Array:getLength()
  return self.elements
end